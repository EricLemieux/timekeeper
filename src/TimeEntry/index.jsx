import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const propTypes = {
  title: PropTypes.string,
  totalTime: PropTypes.number,
  buttonAction: PropTypes.func,
  isPaused: PropTypes.bool,
  notes: PropTypes.string,
};

const defaultProps = {
  title: '',
  totalTime: 0,
  buttonAction: null,
  isPaused: false,
  notes: '',
};

function TimeEntry({
  title,
  totalTime,
  buttonAction,
  isPaused,
  notes,
}) {
  return (
    <div className="time-entry">
      <button className={`time-entry--button ${isPaused ? 'time-entry--button__paused' : ''}`} onClick={buttonAction}>{isPaused ? 'Play' : 'Pause'}</button>
      <span className="time-entry--title">{title}</span>
      <span className="time-entry--time">{totalTime.toFixed(2)} H</span>
      <div className="time-entry--notes">{notes}</div>
    </div>
  );
}

TimeEntry.propTypes = propTypes;

TimeEntry.defaultProps = defaultProps;

export default TimeEntry;
