import React, { Component } from 'react';
import './App.css';
import TimeEntry from './TimeEntry';

class Time {
  constructor({ ...props }) {
    this.id = props.id || Math.floor(Math.random() * 1000000);
    this.title = props.title || '';
    this.notes = props.notes || '';
    this.totalTime = props.totalTime || 0;
    this.timeEllapsed = 0;
    this.isPaused = false;
    this.startingTime = new Date();
  }
}

function getHoursFromDateDifference(diff) {
  return diff / (1000 * 60 * 60);
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      latestEntryId: 1,
      timeEntries: [],
    };

    setInterval(() => {
      const currentTime = new Date();
      this.setState({
        timeEntries: this.state.timeEntries.map((e) => {
          const tmp = e;

          if (!e.isPaused) {
            tmp.timeEllapsed = getHoursFromDateDifference(currentTime - tmp.startingTime);
          }

          return tmp;
        }),
      });
    }, 1000);

    this.createTimeEntry = this.createTimeEntry.bind(this);
  }

  createTimeEntry(e) {
    e.preventDefault();

    const title = e.target.querySelector('#title').value;
    const notes = e.target.querySelector('#notes').value;

    const newTime = new Time({
      id: this.state.latestEntryId,
      title,
      notes,
    });

    e.target.querySelector('#title').value = '';
    e.target.querySelector('#notes').value = '';

    this.setState({
      latestEntryId: this.state.latestEntryId + 1,
      timeEntries: [...this.state.timeEntries, newTime],
    });
  }

  render() {
    return (
      <div className="App">
        <form onSubmit={this.createTimeEntry}>
          <label htmlFor="title">
            <span className="form-label">Title</span>
            <input type="text" id="title" name="title" />
          </label>
          <label htmlFor="notes">
            <span className="form-label">Notes</span>
            <input type="textarea" id="notes" name="notes" />
          </label>
          <button type="submit">Add</button>
        </form>

        <div className="time-entries">
          {this.state.timeEntries.map(t => (
            <TimeEntry
              key={t.id}
              totalTime={t.totalTime + t.timeEllapsed}
              title={t.title}
              notes={t.notes}
              isPaused={t.isPaused}
              buttonAction={() => {
                const index = this.state.timeEntries.findIndex(ent => ent.id === t.id);

                const tmp = this.state.timeEntries;

                if (tmp[index].isPaused) {
                  tmp[index].startingTime = new Date();
                } else {
                  tmp[index].totalTime += tmp[index].timeEllapsed;
                  tmp[index].timeEllapsed = 0;
                }

                tmp[index].isPaused = !tmp[index].isPaused;

                this.setState({
                  timeEntries: tmp,
                });
              }}
            />))}
        </div>
      </div>
    );
  }
}

export default App;
